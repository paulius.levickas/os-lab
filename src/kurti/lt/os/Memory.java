package kurti.lt.os;

public class Memory {

    private static final int WORD_SIZE = 4;
    private static final int WORD_COUNT = 4096;
    private static final int BLOCK_SIZE = 16;
    private static final int VM_BLOCK_COUNT = 224;

    private static final int SUPERVISOR = WORD_COUNT - (16 * BLOCK_SIZE); //RODO I SUPERVISOR MEMORY PRADZIA: 3840
    private static final int PAGING = SUPERVISOR - (16 * BLOCK_SIZE); //POINTS TO THE BEGINING OF PAGING MEMORY         : 3584
    private static final int SHARED = SUPERVISOR - (2 * BLOCK_SIZE); //POINTS TO THE BEGINING OF SHARED MEMORY          : 3808

    private static char[][] memory = new char[WORD_COUNT][WORD_SIZE];   // [4096][4]
    private static boolean[] memoryLocks = new boolean[VM_BLOCK_COUNT];
    private static boolean[] sharedMemLocks = new boolean[2];

    public static void fakeMemoryLocks(){
        for(int i = 0; i < memoryLocks.length; i++){
            if(i%2 == 0){
                memoryLocks[i] = true;
            }
        }
    }

    public static int getEmptyBlockAddress() {
        for(int i = 0; i < memoryLocks.length; i++) {
            if(!memoryLocks[i]) {
                memoryLocks[i] = true;
                return i*BLOCK_SIZE;
            }
        }
        return -1;
    }

    public static void lockSharedBlock(int blockIndex){
        sharedMemLocks[blockIndex] = true;
    }

    public static void unlockSharedBlock(int blockIndex){
        sharedMemLocks[blockIndex] = false;
    }

    public static boolean checkShrMemoryLock(int address){
        return !sharedMemLocks[address / BLOCK_SIZE];
    }

    public static void addToMemory(int address, char[] word) {
        memory[address] = word;
    }

    public static void addToSupervisor(int address, char[] word) {
        memory[SUPERVISOR + address] = word;
    }

    public static int getSharedMemoryStart(){
        return SHARED;
    }

    public static int getPagingMemoryStart() {
        int address = PAGING;
        while(memory[address][0] != 0 && address < SUPERVISOR) {
            address += BLOCK_SIZE;
        }
        return address;
    }

    public static char[][] getMemory() {
        return memory;
    }

    public static char [] getMemory(int address) {
        return memory[address];
    }

    public static int getMemoryInt(int address){
        return Integer.parseInt(String.valueOf(memory[address]), 16);
    }

    public static char[] getFromSupervisor(int address) {
        return memory[SUPERVISOR + address];
    }

    public static void setMemoryLocks(boolean[] memoryLocks) {
        Memory.memoryLocks = memoryLocks;
    }

    public static char[] getStartOfBlock(int blockIndex) {
        if(blockIndex < 0 || blockIndex > (WORD_COUNT / BLOCK_SIZE) - 1) {
            throw new IllegalArgumentException("Incorrect block specified!");
        } else {
            return memory[blockIndex * BLOCK_SIZE];
        }
    }
}

