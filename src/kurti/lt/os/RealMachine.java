package kurti.lt.os;

import java.io.*;
import java.util.*;


public class RealMachine {
    private int ra;      //bendros paskirties pirmasis registras
    private int rb;      //bendros paskirties antrasis registras

    private short ic;    //virtualios masinos programos skaitiklis
    private byte sf;     //pozymiu registras

    private int ptr;     //puslapiu lenteles registras

    private byte pi;     //programiniu pertrukimu registras
    private byte si;     //supervizoriniu pertraukimu registras
    private byte ti;     //taimerio registras

    private int cm;     //registras rodantis i bendra atminti

    private byte mode;   //registras nusakantis procesoriaus darbo rezima (0 - supervizorinis; 1 - vartotojas)
    private byte ch1;    //pirmasis kanalu registras jungiantis klaviatura su vartotojo atmintimi
    private byte ch2;    //antrasis kanalu registras jungiantis ekrana su vartotojo atmintimi
    private byte ch3;    //treciasis kanalu registras jungiantis kietaji diska su vartotojo amtintimi

    private final String HDD = "/Users/pauliuslevickas/Documents/University/OS/MUSU/hdd.txt";
    private final String FLASH = "/Users/pauliuslevickas/Documents/University/OS/MUSU/flash.txt";
    private ArrayList<String> progNames = new ArrayList<>();

    private boolean DEBUG = true;

    RealMachine(){
        ra = 0;
        rb = 0;
        ic = 0;
        sf = 0;
        ptr = 0;
        ti = 10;

        cm = Memory.getSharedMemoryStart();

        mode = 0;
        ch1 = 0;
        ch2 = 0;
        ch3 = 0;
        startMenu();
    }

    public void startMenu(){
        while(true) {
            System.out.println();
            System.out.println("MENU");
            System.out.println("1. RUN VM");
            System.out.println("2. PRINT ALL REGISTERS");
            System.out.println("3. PRINT MEMORY");
            System.out.println("4. MOUNT FLASH");
            System.out.println("10. EXIT");
            Scanner input = new Scanner(System.in);
            int value = input.nextInt();

            switch (value) {
                case 1 :
                    System.out.println("Running programs from HDD: ");

                    this.progNames.add("$prog1");
                    this.progNames.add("$prog2");
                    this.progNames.add("$prog3");

                    for(int i=0; i<progNames.size(); i++){
                        runVM(progNames.get(i));
                    }

                    break;
                case 2 :
                    printRegisters();
                    break;
                case 3 :
                    printMemory();
                    break;
                case 4:
                    mountFlash();
                    break;
                case 10 :
                    System.exit(0);
                    break;
                default :
                    System.out.println("BAD INPUT!!!");
                    break;
            }
        }
    }



    public void mountFlash(){
        System.out.println("\nLoading programs...");
        int[] newProgramCount = {0};

        try{
            BufferedWriter hddWriter = new BufferedWriter(new FileWriter(this.HDD, true));
            BufferedReader hddReader = new BufferedReader(new FileReader(this.HDD));
            BufferedReader flashReader = new BufferedReader(new FileReader(this.FLASH));
            List<String> loadedPrograms = getLoadedPrograms(hddReader);
            StringBuilder carryName = new StringBuilder();

            hddWriter.newLine();

            while(flashReader.ready()) {
                if(!loadNewProgram(loadedPrograms, flashReader, hddWriter, carryName, newProgramCount)) {
                    return;
                }
            }

            hddWriter.close();
            flashReader.close();
            System.out.println("Loaded " + newProgramCount[0] + " programs");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean loadNewProgram(List<String> loadedPrograms, BufferedReader reader, BufferedWriter writer,
                                StringBuilder carryName, int[] newProgramCount) throws  IOException {
        String newProgram = skipToNewProgram(reader, loadedPrograms, carryName);
        if (newProgram == null) {
            System.out.println("No new programs found");
            return false;
        }
        newProgramCount[0]++;
        writer.write(newProgram);
        writer.newLine();

        while(reader.ready()) {
            String newLine = reader.readLine();
            if (newLine.contains("$prog")) {
                if (loadedPrograms.contains(newLine)) {
                    carryName.setLength(0);
                    return true;
                } else {
                    carryName.append(newLine);
                    return true;
                }
            }
            writer.write(newLine);
            writer.newLine();
        }
        return true;
    }

    private List<String> getLoadedPrograms(BufferedReader reader) throws IOException {
        List<String> programs = new ArrayList<>();
        while(reader.ready()) {
            String newLine = reader.readLine();
            if(newLine.contains("$prog")) {
                programs.add(newLine);
            }
        }
        reader.close();
        return programs;
    }

    private String skipToNewProgram(BufferedReader reader, List<String> loadedPrograms, StringBuilder carryName) throws IOException {
        if(carryName.length() > 0) {
            if(!loadedPrograms.contains(carryName.toString())) {
                String result = carryName.toString();
                carryName.setLength(0);
                return result;
            }
        }

        while(reader.ready()) {
            String newLine = reader.readLine();
            if(newLine.contains("$prog")) {
                if(!loadedPrograms.contains(newLine)) {
                    return newLine;
                }
            }
        }
        return null;
    }

    public void printMemory() {
        System.out.print("0::");
        for(int i = 0; i < Memory.getMemory().length - 1; ++i) {
            System.out.print(String.format("%1$4s", String.valueOf(Memory.getMemory(i)) + "|"));
            if((i+1)%16==0){
                System.out.println();
                System.out.print(Integer.toHexString(i+1).toUpperCase() + ":: ");
            }
        }
        System.out.println();
    }

    public void printRegisters() {
        System.out.println("---------REGISTERS--------");
        System.out.println("RA: " + Integer.toHexString(getRa()));
        System.out.println("RB: " + Integer.toHexString(getRb()));
        System.out.println("IC: " + Integer.toHexString(getIc()));
        System.out.println("SF: " + Integer.toHexString(getSf()));
        System.out.println("PTR: " + Integer.toHexString(this.ptr));
        System.out.println("------");
        System.out.println("MODE: " + Integer.toHexString(this.mode));
        System.out.println("CH1: " + Integer.toHexString(this.ch1));
        System.out.println("CH2: " + Integer.toHexString(this.ch2));
        System.out.println("CH3: " + Integer.toHexString(this.ch3));
        System.out.println("--------------------------");
    }

    public void runVM(String codeName){
        try{
            clearSupervisorMemory();

            loadCode(codeName);

            checkInterrupts();

            //puslapiavimas
            memoryAllocation();
            addCodeToVM();
            //----------------

            setMode((byte)1);

            VirtualMachine vm = new VirtualMachine(this);
            readCode(vm);

            setMode((byte) 0);
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    private void clearSupervisorMemory(){

        if(this.DEBUG){
            System.out.println("\nEsam clearSuperMem...");
        }

        ra = 0;
        rb = 0;
        ic = 0;
        sf = 0;
        ptr = 0;
        ti = 10;

        cm = Memory.getSharedMemoryStart();

        mode = 0;
        ch1 = 0;
        ch2 = 0;
        ch3 = 0;
        si = 0;
        pi = 0;

        int index = 0;

        while(Memory.getFromSupervisor(index)[0] != 0
                || Memory.getFromSupervisor(index)[1] != 0
                || Memory.getFromSupervisor(index)[2] != 0
                || Memory.getFromSupervisor(index)[3] != 0){
            char[] filler = new char[4];
            Memory.addToSupervisor(index, filler);
            index++;
        }
    }

    public void loadCode(String codeName) throws Exception{
        setMode((byte) 0);
        setCh3((byte) 1);

        String line;
        String[] tokens;
        try(BufferedReader br = new BufferedReader(new FileReader(this.HDD))) {
            int wordIndex = 0;

            //Goes through the file looking for a codeName
            //If codeName is not found gives "Wrong assignment" exception
            while(!(line = br.readLine()).equals(codeName)){

            }

            while((line = br.readLine()) != null){
                if(line.equals("")){
                    break;
                }
                //Checks if code is not too big
                if(wordIndex >= 128) {
                    throw new Exception("4");
                }

                if(checkCode(line)){
                    //DW and DD procedures are unique in a sense, that they take up 2 words
                    if(line.matches("[D][W|D]\\s.+")){
                        tokens = line.split(" ");
                        Memory.addToSupervisor(wordIndex, tokens[0].toCharArray());
                        wordIndex++;

                        if(wordIndex >= 128) {
                            throw new Exception("4");
                        }
                        if(tokens[1].length() > 4) {
                            Memory.addToSupervisor(wordIndex, tokens[1].substring(0, 4).toCharArray());
                            wordIndex++;
                            if(wordIndex >= 128) {
                                throw new Exception("4");
                            }
                            Memory.addToSupervisor(wordIndex, tokens[1].substring(4).toCharArray());
                            wordIndex++;
                        } else {
                            Memory.addToSupervisor(wordIndex, tokens[1].toCharArray());
                            wordIndex++;
                        }
                    } else {
                        Memory.addToSupervisor(wordIndex, line.toCharArray());
                        wordIndex++;
                    }
                }
            }
            if(!String.valueOf(Memory.getFromSupervisor(wordIndex - 1)).equals("HALT")){
                throw new Exception("1");
            }
        } catch (FileNotFoundException fe) {
            throw new Exception("HDD DOESN'T EXIST!");
        } catch (NullPointerException e){
            throw new Exception("CODE WITH THAT NAME WAS NOT FOUND ON HDD");
        } catch (Exception e) {
            if(e.getMessage().substring(0, 1).matches("\\d")) {
                setPi(Byte.parseByte(e.getMessage().substring(0, 1)));
            }
        }
        setMode((byte) 1);
        setCh3((byte) 0);
    }

    private boolean checkCode(String token) throws Exception{
        switch (token) {
            case "CODE":
                return true;
            case "ADD" :
                return true;
            case "SUB" :
                return true;
            case "CMP" :
                return true;
            case "HALT" :
                return true;
            case "PRTS" :
                return true;
            case "PRTN" :
                return true;
            case "READ" :
                return true;
            case "DATA" :
                return true;
            case "DN" :
                return true;
            default:
                break;
        }

        if(token.matches("[M][A|B](\\d|[A-F])(\\d|[A-F])")){
            if(Integer.parseInt(token.substring(2), 16) >= 128
                    && Integer.parseInt(token.substring(2), 16) <= 255) {
                return true;
            }
            throw new Exception("3" + token);
        } else if(token.matches("[A|B][M](\\d|[A-F])(\\d|[A-F])")){
            if(Integer.parseInt(token.substring(2), 16) >= 128
                    && Integer.parseInt(token.substring(2), 16) <= 255) {
                return true;
            }
            throw new Exception("3" + token);
        } else if(token.matches("(([S][A])|([A][S]))(\\d|[A-F])(\\d|[A-F])")){
            if(Integer.parseInt(token.substring(2), 16) >= 0
                    && Integer.parseInt(token.substring(2), 16) <= 31) {
                return true;
            }
            throw new Exception("3" + token);
        } else if(token.matches("[U|L][C].")){
            if(token.matches("..(\\d|[A-F])")){
                return true;
            }
            throw new Exception("3" + token);
        } else if(token.matches("[J][P|E|B|A](\\d|[A-F])(\\d|[A-F])")){
            if(Integer.parseInt(token.substring(2), 16) >= 0
                    && Integer.parseInt(token.substring(2), 16) <= 127) {
                return true;
            }
            throw new Exception("3" + token);
        } else if(token.matches("[P](\\d|[A-F]){3}")) {
            if(Integer.parseInt(token.substring(2, 3), 16) <= Integer.parseInt(token.substring(3, 4), 16)
                    && Integer.parseInt(token.substring(2,3), 16) <= 15
                    && Integer.parseInt(token.substring(1,2), 16) >= 8
                    && Integer.parseInt(token.substring(1,2), 16) <= 15) {
                return true;
            }
            throw new Exception("3" + token);
        } else if(token.matches("[D][W]\\s(\\d|[A-F]){1,8}")) {
            return true;
        } else if(token.matches("[D][D]\\s\\w{1,4}")){
            return true;
        }
        throw new Exception("1");
    }

    public void memoryAllocation() {
        this.ptr = Memory.getPagingMemoryStart();
        Memory.fakeMemoryLocks();
        for(int i = 0; i < 16; i++) {
            Memory.addToMemory(ptr+i, Integer.toHexString(Memory.getEmptyBlockAddress()).toCharArray());
        }
    }

    private void addCodeToVM(){
        int count = 0;
        for(int i = ptr; i < ptr + 8; i++) {
            if(Memory.getFromSupervisor(count)[0] != 0){
                for(int j = 0; j < 16; ++j){
                    Memory.addToMemory(Memory.getMemoryInt(i) + j, Memory.getFromSupervisor(count));
                    count++;
                }
            } else {
                return;
            }
        }
    }

    public void readCode(VirtualMachine vm) throws Exception {
        while(!String.valueOf(getWordFromMemory(ic)).equals("HALT")){

            vm.execute(String.valueOf(getWordFromMemory(ic)));
            checkInterrupts();
            increaseIc();
        }
        if(String.valueOf(getWordFromMemory(ic)).equals("HALT")){
            setSi((byte) 5);        //Setting HALT interrupt
        }
    }

    public void checkInterrupts() throws Exception{
        ti--;
        switch (this.pi) {
            case 1:
                setMode((byte) 0);
                throw new Exception("INCORRECT OPERATION CODE! VM TERMINATING");
            case 2:
                setMode((byte) 0);
                throw new Exception("INCORRECT ASSIGNMENT! VM TERMINATING");
            case 3:
                setMode((byte) 0);
                throw new Exception("INCORRECT ADDRESS! VM TERMINATING");
            case 4:
                setMode((byte) 0);
                throw new Exception("OVERFLOW! VM TERMINATING");
            default:
                break;
        }
        switch (this.si) {
            case 1:
                handlePRTS();
                ti -= 2;
                break;
            case 2:
                handlePRTN();
                ti -= 2;
                break;
            case 3:
                handleP();
                ti -= 2;
                break;
            case 4:
                handleREAD();
                ti -= 2;
                break;
            case 5:
                System.out.print("HALT INTERRUPT");
                throw new Exception("HALT INTERRUPT");
            case 6:
                handleLC();
                break;
            case 7:
                handleUC();
                break;
            default:
                break;
        }
        if(ti <= 0){
            setMode((byte) 0);
            System.out.println("TIMER INTERRRUPT");
            ti=30;
            setMode((byte) 1);
        }
    }

    private void handlePRTS(){
        setMode((byte) 0);
        setCh2((byte) 1);
        System.out.println("            ++SPAUZDINTUVAS++");
        System.out.println("|                                     |");
        System.out.println("RA:" + String.valueOf(encodeToAscii(ra)));
        System.out.println("|                                     |");
        System.out.println("---------------------------------------");
        setCh2((byte) 0);
        si = 0;
        setMode((byte) 1);
    }

    private void handlePRTN(){
        setMode((byte) 0);
        setCh2((byte) 1);
        System.out.println("            ++SPAUZDINTUVAS++");
        System.out.println("|                                     |");
        System.out.println("RA:" + ra);
        System.out.println("|                                     |");
        System.out.println("            ++SPAUZDINTUVAS++");
        setCh2((byte) 0);
        si = 0;
        setMode((byte) 1);
    }

    private void handleP() {
        setMode((byte) 0);
        int block = Integer.parseInt(String.valueOf(getWordFromMemory(ic)).substring(1, 2), 16);
        int y = Integer.parseInt(String.valueOf(getWordFromMemory(ic)).substring(2, 3), 16);
        int z = Integer.parseInt(String.valueOf(getWordFromMemory(ic)).substring(3, 4), 16);
        setCh2((byte) 1);

        System.out.println("             ++SPAUZDINTUVAS++");
        System.out.println("|                                     |");
        for(int i = y; i <= z; i++) {
            System.out.print(getWordFromMemory(block*16 + i));
        }
        System.out.println();
        System.out.println("|                                     |");
        System.out.println("            ++SPAUZDINTUVAS++");
        setCh2((byte) 0);
        si = 0;
        setMode((byte) 1);
    }

    private void handleREAD(){
        setMode((byte) 0);
        setCh1((byte) 1);
        System.out.println("Enter a number: ");
        Scanner input = new Scanner(System.in);
        String value = input.nextLine();
        if(value.length() > 8){
            setPi((byte) 4);
            return;
        } else if(!value.matches("\\p{XDigit}{1,8}")) {
            setPi((byte) 2);
            return;
        }
        setRa(Integer.parseInt(value, 16));
        //setSi((byte) 0);
        setCh1((byte) 0);
        si = 0;
        setMode((byte) 1);
    }

    private void handleLC(){
        setMode((byte) 0);
        int index = Integer.parseInt(String.valueOf(getWordFromMemory(ic)).substring(2), 16);
        if(index >= 0 && index <= 1){
            Memory.lockSharedBlock(index);
        } else setPi((byte) 3);
        si = 0;
        setMode((byte) 1);
    }

    private void handleUC(){
        setMode((byte) 0);
        int index = Integer.parseInt(String.valueOf(getWordFromMemory(ic)).substring(2), 16);
        if(index >= 0 && index <= 1){
            Memory.unlockSharedBlock(index);
        } else setPi((byte) 3);
        si = 0;
        setMode((byte) 1);
    }

    public void addWordToMemory(char [] word){
        for(int i = ptr + 8; i < ptr + 16; ++i) {
            for(int j = 0; j < 16; ++j){
                if(Memory.getMemory(Memory.getMemoryInt(i) + j)[0] == 0
                        && Memory.getMemory(Memory.getMemoryInt(i) + j)[1] == 0
                        && Memory.getMemory(Memory.getMemoryInt(i) + j)[2] == 0
                        && Memory.getMemory(Memory.getMemoryInt(i) + j)[3] == 0) {
                    Memory.addToMemory(Memory.getMemoryInt(i) + j, word);
                    return;
                }
            }
        }
    }

    public void addWordToMemory(char [] word, int index){
        if(index <= 255 && index >= 128) {
            Memory.addToMemory(Memory.getMemoryInt(ptr + index/16) + index%16, word);
        } else {
            setPi((byte) 2);
        }
    }

    public void addWordToSharedMemory(char [] word, int index){
        if(index >= 0 && index < 32) {
            if(Memory.checkShrMemoryLock(index)){
                Memory.addToMemory(cm + index, word);
            } else setPi((byte) 2);
        } else setPi((byte) 2);
    }

    public char[] getWordFromMemory(int index){
        if(index <= 255) {
            return Memory.getMemory(Memory.getMemoryInt(ptr + index/16) + index%16);
        } else {
            setPi((byte) 2);
            return null;
        }
    }

    public char[] getWordFromSharedMemory(int index){
        if(index >= 0 && index < 32) {
            return Memory.getMemory(cm + index);
        } else {
            setPi((byte) 2);
            return null;
        }
    }

    public char[] encodeToAscii(int number){
        char [] codedNumber = new char[4];
        String hexNumber = Integer.toHexString(number);
        if(hexNumber.length()%2 != 0){
            hexNumber = "0" + hexNumber;
        }
        String [] bytes = hexNumber.split("(?<=\\G.{2})");

        int byteIndex = 0;
        for(int i = 4 - bytes.length; i < 4; i++){

            codedNumber[i] = (char) Integer.parseInt(bytes[byteIndex], 16);
            byteIndex++;

        }
        return codedNumber;
    }

    public char[] encodeToAscii(String hexNumber){
        char [] codedNumber = new char[4];
        if(hexNumber.length()%2 != 0){
            hexNumber = "0" + hexNumber;
        }
        String [] bytes = hexNumber.split("(?<=\\G.{2})");

        int byteIndex = 0;
        for(int i = 4 - bytes.length; i < 4; i++){
            codedNumber[i] = (char) Integer.parseInt(bytes[byteIndex], 16);
            byteIndex++;
        }
        System.out.println(codedNumber);
        return codedNumber;
    }

    public int decodeFromAscii(char [] word){
        String decoded = new String();
        if (Long.parseLong(Integer.toString((int) word[0], 16), 16) >= 128) {
            for(int i = 0; i < word.length; ++i) {
                int temp = ~(int) Long.parseLong(Integer.toString((int) word[i], 16), 16);
                //temp = ~temp;
                String s = Integer.toHexString(temp);
                decoded = decoded + s.substring(s.length() - 2);
            }
            return (-1) * (Integer.parseInt(decoded, 16) + 1);
        }
        else {
            for(int i = 0; i < word.length; ++i) {
                decoded = decoded + String.format("%02x", (int) word[i]);
            }
        }

        if(decoded.isEmpty()){
            return 0;
        }
        return Integer.parseInt(decoded, 16);
    }

    public int getRa() {
        return ra;
    }

    public int getRb() {
        return rb;
    }

    public short getIc() {
        return ic;
    }

    public byte getSf() {
        return sf;
    }

    public void setRa(int ra) {
        this.ra = ra;
    }

    public void setRb(int rb) {
        this.rb = rb;
    }

    public void setIc(short ic) {
        this.ic = ic;
    }

    public void increaseIc() {
        this.ic++;
    }

    public void setSf(byte sf) {
        this.sf = sf;
    }

    public void setZF(boolean status){
        if(status) {
            sf = (byte) (sf | (1 << 2));
        } else {
            sf = (byte) (sf & ~(1 << 2));
        }
    }

    public boolean getZF(){
        return (((sf >> 2) & 1) == 1);
    }

    public void setCF(boolean status){
        if(status) {
            sf = (byte) (sf | (1 << 1));
        } else {
            sf = (byte) (sf & ~(1 << 1));
        }
    }

    public boolean getCF(){
        return (((sf >> 1) & 1) == 1);
    }

    public void setMode(byte mode) {
        this.mode = mode;
        if(mode == 0){
            System.out.println("MODE CHANGED TO SUPERVISOR");
        } else {
            System.out.println("MODE CHANGED TO USER");
        }
    }

    public void setCh1(byte ch1) {
        this.ch1 = ch1;
    }

    public void setCh2(byte ch2) {
        this.ch2 = ch2;
    }

    public void setCh3(byte channel) {
        this.ch3 = channel;
    }

    public void setPi(byte pi) {
        setMode((byte) 0);
        this.pi = pi;
        setMode((byte) 1);
    }

    public void setSi(byte si) {
        setMode((byte) 0);
        this.si = si;
        setMode((byte) 1);
    }
}