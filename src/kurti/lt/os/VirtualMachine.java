package kurti.lt.os;

public class VirtualMachine {

    private RealMachine realMachine;

    VirtualMachine(RealMachine realMachine) {
        this.realMachine = realMachine;
    }

    public void execute(String process){
        System.out.println(process);
        switch(process) {
            case "DATA" :
                System.out.println("DATA MOVING PROCESS START");
                break;
            case "DW" :
                processDW();
                break;
            case "DD" :
                processDD();
                break;
            case "CODE" :
                System.out.println("CODE START");
                break;
            case "ADD":
                processADD();
                break;
            case "SUB":
                processSUB();
                break;
            case "CMP":
                processCMP();
                break;
            case "PRTS":
                processPRTS();
                break;
            case "PRTN":
                processPRTN();
                break;
            case "READ":
                processREAD();
                break;
            default:
                break;
        }

        if(process.matches("[M][A](\\d|[A-F]){2}")){
            processMA(Integer.parseInt(process.substring(2), 16));
        } else if(process.matches("[M][B](\\d|[A-F]){2}")){
            processMB(Integer.parseInt(process.substring(2), 16));
        } else if(process.matches("[A][M](\\d|[A-F]){2}")){
            processAM(Integer.parseInt(process.substring(2), 16));
        } else if(process.matches("[B][M](\\d|[A-F]){2}")){
            processBM(Integer.parseInt(process.substring(2), 16));
        } else if(process.matches("[S][A](\\d|[A-F])(\\d|[A-F])")){
            processSA(Integer.parseInt(process.substring(2), 16));
        } else if(process.matches("[A][S](\\d|[A-F])(\\d|[A-F])")){
            processAS(Integer.parseInt(process.substring(2), 16));
        } else if(process.matches("[U][C].")){
            processUC();
        } else if(process.matches("[L][C].")){
            processLC();
        } else if(process.matches("[P](\\d|[A-F]){3}")) {
            processP();
        } else if(process.matches("[J][P|E|B|A|C|NB](\\d|[A-F])(\\d|[A-F])")){
            processJMP(process.substring(0,2), Integer.parseInt(process.substring(2), 16));
        }
    }

    private void processDW(){
        realMachine.increaseIc();
        String strNum = String.valueOf(realMachine.getWordFromMemory(realMachine.getIc()));
        if(String.valueOf(realMachine.getWordFromMemory(realMachine.getIc() + 1)).matches("(\\d|[A-F]){1,4}")
                && String.valueOf(realMachine.getWordFromMemory(realMachine.getIc() + 2)).matches("([D][D|W])|([C][O][D][E])")){
            realMachine.increaseIc();
            strNum =  strNum + String.valueOf(realMachine.getWordFromMemory(realMachine.getIc()));
        }
        //int number = Integer.parseInt(String.valueOf(realMachine.getWordFromMemory(realMachine.getIc())), 16);
        realMachine.addWordToMemory(realMachine.encodeToAscii(strNum));
    }

    private void processDD(){
        realMachine.increaseIc();
        realMachine.addWordToMemory(realMachine.getWordFromMemory(realMachine.getIc()));
    }

    private void processMA(int address){
        System.out.println(realMachine.decodeFromAscii(realMachine.getWordFromMemory(address)));

        realMachine.setRa(realMachine.decodeFromAscii(realMachine.getWordFromMemory(address)));
        System.out.println("RA: " + realMachine.getRa());
    }

    private void processMB(int address){
        realMachine.setRb(realMachine.decodeFromAscii(realMachine.getWordFromMemory(address)));
        System.out.println("RB: " + realMachine.getRb());
    }

    private void processAM(int address){
        realMachine.addWordToMemory(realMachine.encodeToAscii(realMachine.getRa()), address);
    }

    private void processBM(int address){
        realMachine.addWordToMemory(realMachine.encodeToAscii(realMachine.getRb()), address);
    }

    private void processPRTS(){
        realMachine.setSi((byte) 1);
    }

    private void processPRTN(){
        realMachine.setSi((byte) 2);
    }

    private void processP(){
        realMachine.setSi((byte) 3);
    }

    private void processREAD(){
        realMachine.setSi((byte) 4);
    }

    private void processADD(){
        realMachine.setRa(realMachine.getRb() + realMachine.getRa());

        System.out.println("RA after ADD: " + realMachine.getRa());
    }

    private void processSUB(){
        realMachine.setRa(realMachine.getRa() - realMachine.getRb());
        System.out.println("RA after SUB: " + realMachine.getRa());
    }

    private void processCMP(){
        if(realMachine.getRa() == realMachine.getRb()) {
            realMachine.setZF(true);
            System.out.println("CMP: RA = RB");
        } else if(realMachine.getRa() < realMachine.getRb()) {
            realMachine.setCF(true);
            System.out.println("CMP: RA < RB");
        } else if(realMachine.getRa() > realMachine.getRb()) {
            realMachine.setCF(false);
            System.out.println("CMP: RA > RB");
        }
    }

    private void processSA(int index){
        realMachine.setRa(realMachine.decodeFromAscii(realMachine.getWordFromSharedMemory(index)));
    }

    private void processAS(int index){
        realMachine.addWordToSharedMemory(realMachine.encodeToAscii(realMachine.getRa()), index);
    }

    private void processLC(){
        realMachine.setSi((byte) 6);
    }

    private void processUC(){
        realMachine.setSi((byte) 7);
    }

    private void processJMP(String type, int address) {
        if ((type.equals("JP"))
                || (type.equals("JE") && realMachine.getZF())
                || (type.equals("JB") && realMachine.getCF())
                || (type.equals("JNB") && !realMachine.getZF())
                || (type.equals("JC") && realMachine.getCF())
                || (type.equals("JA") && !realMachine.getZF() && !realMachine.getCF())) {
            realMachine.setIc((short) (address - 1));
        }
    }

}

